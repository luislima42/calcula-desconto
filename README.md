# Integração e entrega continua

Desenvolvimento de Site, uso do GitLab e Teste Automatizado

Grupo: 
- Felipe Soares
- Lucas Chaves 
- Lucas Oliveira
- Luis Felipe

Resumo sobre o conceito de PipeLine

No GitLab, um "pipeline" é uma parte fundamental do sistema de integração contínua (CI) e entrega contínua (CD) que a plataforma oferece. Um pipeline é essencialmente uma série de etapas automatizadas que são executadas quando um novo código é enviado para um repositório GitLab. Essas etapas podem incluir compilação, testes automatizados, implantação e outros processos relacionados ao desenvolvimento de software.
 
Aqui está um breve resumo do conceito de pipeline no GitLab:
O pipeline de CI no GitLab é acionado sempre que um novo código é enviado para o repositório. Ele automatiza a compilação do código-fonte, a execução de testes e verificações para garantir que o código esteja livre de erros.
Os pipelines do GitLab são altamente configuráveis e podem ser adaptados às necessidades específicas do projeto. Os desenvolvedores podem definir scripts e instruções para controlar o comportamento do pipeline.
Os pipelines no GitLab oferecem visibilidade sobre o progresso das etapas de CI/CD. Os desenvolvedores podem monitorar o status e as saídas de cada etapa do pipeline.
Além da CI, o GitLab permite a automação de processos de CD por meio de pipelines. Isso inclui a implantação de código em ambientes de teste e produção após a aprovação dos testes de CI.
Os pipelines do GitLab fazem parte de uma abordagem DevOps, permitindo uma integração contínua e eficiente entre desenvolvimento e operações.
O GitLab oferece uma variedade de recursos, como integração com contêineres e orquestração de contêineres, que podem ser incorporados aos pipelines para automação de implantações em contêineres.
Os pipelines no GitLab são uma ferramenta poderosa para automatizar e facilitar o processo de desenvolvimento de software, garantindo que as alterações de código sejam testadas e implantadas de forma consistente, reduzindo erros e melhorando a eficiência do desenvolvimento.
